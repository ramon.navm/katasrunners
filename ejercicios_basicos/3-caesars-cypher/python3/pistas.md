# Pistas

## Pista 1

Usa `ord()` para convertir los caracteres en inglés a ASCII. [Link](https://docs.python.org/3/library/functions.html#ord)

## Pista 2

Usa `chr()` para convertir el ASCII a caracteres en inglés. [Link](https://docs.python.org/3/library/functions.html#chr)

## Pista 3

Deja cualquier cosa que no este entre A-Z sin tocar.

