# Implementaciones de katas

¿Has llegado a la parte del itinerario donde te toca hacer katas? Este repositorio es para que subas las implementaciones que hagas de cada kata.

## Cómo usarlo

Crea una carpeta con tu nombre. Dentro, crea una carpeta por cada una de las katas que hagas. Y dentro de esta carpeta, una subcarpeta por cada una de las implementaciones, indicando el lenguaje en que este hecho. Cuando necesites que alguien te revise la implementacion, puedes pasar un enlace al repositorio.

Al final, debería quedar una estructura parecida a esta:

- /MiNombre/FizzBuzz/1-js
- /MiNombre/FizzBuzz/2-js
- /MiNombre/MarsRover/1-py
- /OtroNombre/FizzBuzz/1-js

Si no se te ocurre como afrontar los tests de una kata o no sabes como resolverla, puedes utilizar también este repositorio para ver cómo lo han hecho otras personas.

## Ejercicios básicos

Este repositorio también contiene cuatro katas con ejercicios básicos donde los tests ya están hechos. Si no has hecho una kata nunca, empieza por ellos. Copia los ejercicios en tu carpeta personal, no modifiques el código original.

## Templates

La carpeta templates contiene plantillas para varios lenguajes con instrucciones de como preparar el entorno y con código de ejemplo para que puedas ver como se hacen los tests o como se importan los ficheros. Puedes copiarlos como base para hacer tus propias katas.
